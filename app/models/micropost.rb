class Micropost < ApplicationRecord
    validates :content, length: {maximum: 140}, presence: true
    validates :name, presence: true,
    validates :email, presence: true,
    belongs_to :user
end
